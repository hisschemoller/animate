export default function drawGrid(mainCtx, data, clips) {
  const { backgroundColor, canvas, grid, gridCols, gridRows, gridSize } = data;
  mainCtx.fillStyle = backgroundColor;
  mainCtx.fillRect(0, 0, canvas.width, canvas.height);
  for (let col = 0; col < gridCols; col++) {
    for (let row = 0; row < gridRows; row++) {
      const gridData = grid[col][row];
      if (gridData) {
        const [ isCircle, xPos, yPos, isGrowingCircle] = gridData;
        mainCtx.save();
        mainCtx.transform(1, 0, 0, 1, (col + xPos) * gridSize, (row + yPos) * gridSize);
        let angle = 0;
        if (xPos === 0 && yPos === 1) {
          angle = -90;
        } else if (xPos === 1 && yPos === 0) {
          angle = 90;
        } else if (xPos === 1 && yPos === 1) {
          angle = 180;
        }
        mainCtx.rotate(angle * Math.PI / 180);
        const canvas = isCircle ? clips[isGrowingCircle ? 0 : 1].getCanvas() : clips[2].getCanvas();
        mainCtx.drawImage(canvas, 0, 0);
        mainCtx.restore();
      }
    }
  }
}