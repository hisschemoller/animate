import data from './data.js';
import createClip from './clips/clip.js'
import createBDFClip from './clips/bouncingDotsField.js'
import createCircleClip from './clips/circle.js'
import createRectangleClip from './clips/rectangle.js'
import createPDRClip from './clips/pulsingDotsRectangle.js'
import createRandomMatrixClip from './clips/randomMatrix.js'
import drawGrid from './grid.js'
import { scaleDefault }  from './util.js';

const clips = [];
let isCapture = false;
let scale = 1;
let scaleFunction = scaleDefault;

let canvas;
let ctx;
let frame = 0;
let socket;
let captureCounter = 0;

function setup() {
  scaleFunction(data, scale)
  if (isCapture) {
    socket = io.connect('http://localhost:3006');
  }
  
  canvas = document.createElement('canvas');
  canvas.height = data.canvas.height;
  canvas.width = data.canvas.width;
  document.body.appendChild(canvas);
  ctx = canvas.getContext('2d');
}

function createClips() {
  data.clips.forEach(clipData => {
    switch (clipData.type) {
      case 'pulsingDotsRectangle':
        clips.push(createPDRClip(clipData));
        break;
      case 'circle':
        clips.push(createCircleClip(clipData));
        break;
      case 'randomMatrix':
        clips.push(createRandomMatrixClip(clipData));
        break;
      case 'rectangle':
        clips.push(createRectangleClip(clipData));
        break;
      default:
        clips.push(createClip(clipData));
    }
  });
}

function run() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  clips.forEach(clip => {
    // clip.draw(ctx, frame);
    clip.drawCanvas(frame);
  });
  drawGrid(ctx, data, clips);
  frame += 1;
  requestAnimationFrame(run);
}

function capture() {
  captureCounter++;
  if (captureCounter % 8 !== 0) {
      requestAnimationFrame(capture);
      return;
  }

  ctx.clearRect(0, 0, canvas.width, canvas.height);
  clips.forEach(clip => {
    // clip.draw(ctx, frame);
    clip.drawCanvas(frame);
  });
  drawGrid(ctx, data, clips);
  frame += 1;

  // send canvas to node app
  socket.emit('render-frame', {
    frame,
    file: canvas.toDataURL()
  });
  
  // end if this was the last frame
  if (frame < data.endFrame) {
    requestAnimationFrame(capture);
  } else {
      console.log('done');
  }
}

export default function init(options) {
  isCapture = options.isCapture;
  scale = options.scale;
  scaleFunction = options.scaleFunction;

  setup();
  createClips();
  requestAnimationFrame(isCapture ? capture : run);
}