// 4.3 x 60 = 258 frames per loop
// 640 x 360px = 16 x 9 blocks of 40px

let clip;

const height = 360;
const width = 640;
const loopLength = 4.3;
const numLoops = 33;
const fps = 30;
const framesPerLoop = fps * loopLength;
const gridSize = 40;
const speed = (gridSize * 128 * (1/32)) / framesPerLoop;
// const colors = ['#efbb41', '#da7935', '#b84936', '#68785c'];
// const backgroundColor = '#c089a6';
const colors = ['#0a216c', '#411e46', '#003495', '#5c44b6', '#094e56'];
const backgroundColor = '#01070a';

const data = {
  backgroundColor,
  canvas: { height, width },
  endFrame: Math.ceil(framesPerLoop * numLoops),
  clips: [],
  grid: [[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],],
  gridSize,
  gridCols: 16,
  gridRows: 9,
};
export default data;

const baseClip = {
  type: 'circle',
  items: colors.map(color => { return { color, size: gridSize / colors.length, }}),
  centerX: 0, centerY: 0,
  speed,
  fixedRadius: gridSize,
  x: 0, y: 0, width: gridSize, height: gridSize,
};

data.clips.push({
  ...baseClip,
});
data.clips.push({
  ...baseClip, speed: baseClip.speed * -1,
});

data.clips.push({
  type: 'rectangle',
  items: colors.map(color => { return { color, size: gridSize / colors.length, }}),
  speed,
  x: 0, y: 0, width: gridSize, height: gridSize,
});

let col = 0;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [false,1,1];
data.grid[col][2] = [true,1,0,true];
data.grid[col][3] = [true,1,1,true];
data.grid[col][4] = [false,1,1];
data.grid[col][5] = [false,1,1];
data.grid[col][6] = [true,1,0,true];
data.grid[col][7] = [true,1,1,true];
data.grid[col][8] = [true,1,0,true];

col = 1;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [true,1,0,false];
data.grid[col][2] = [false,1,0];
data.grid[col][3] = [false,0,1];
data.grid[col][4] = [true,1,1,false];
data.grid[col][5] = [true,1,0,false];
data.grid[col][6] = [true,0,1,false];
data.grid[col][7] = [true,0,0,false];
data.grid[col][8] = [false,1,0];

col = 2;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [true,0,0,false];
data.grid[col][2] = [true,0,1,false];
data.grid[col][3] = [true,0,0,false];
data.grid[col][4] = [false,1,0];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [true,1,1,false];
data.grid[col][7] = [false,0,0];
data.grid[col][8] = [true,0,0,true];

col = 3;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [false,0,0];
data.grid[col][2] = [false,0,0];
data.grid[col][3] = [false,0,0];
data.grid[col][4] = [true,0,0,true];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [true,0,1,false];
data.grid[col][7] = [false,1,1];
data.grid[col][8] = [true,1,0,true];

col = 4;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [false,1,1];
data.grid[col][2] = [true,1,0,true];
data.grid[col][3] = [true,1,1,true];
data.grid[col][4] = [true,1,0,true];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [true,1,1,false];
data.grid[col][7] = [true,1,0,false];
data.grid[col][8] = [false,1,0];

col = 5;
data.grid[col][0] = [false,0,1];
data.grid[col][1] = [true,1,1,false];
data.grid[col][2] = [true,0,0,true];
data.grid[col][3] = [false,0,1];
data.grid[col][4] = [true,0,1,false];
data.grid[col][5] = [true,0,0,false];
data.grid[col][6] = [false,1,0];
data.grid[col][7] = [false,0,1];
data.grid[col][8] = [false,1,0];

col = 6;
data.grid[col][0] = [false,0,1];
data.grid[col][1] = [true,0,1,false];
data.grid[col][2] = [true,1,0,true];
data.grid[col][3] = [true,0,1,true];
data.grid[col][4] = [false,0,0];
data.grid[col][5] = [false,0,0];
data.grid[col][6] = [true,0,0,true];
data.grid[col][7] = [false,0,1];
data.grid[col][8] = [false,1,0];

col = 7;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [true,1,0,false];
data.grid[col][2] = [false,1,0];
data.grid[col][3] = [true,1,1,true];
data.grid[col][4] = [false,1,1];
data.grid[col][5] = [false,1,1];
data.grid[col][6] = [false,1,1];
data.grid[col][7] = [true,0,0,false];
data.grid[col][8] = [false,1,0];

col = 8;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [true,0,0,false];
data.grid[col][2] = [true,0,1,false];
data.grid[col][3] = [true,0,0,false];
data.grid[col][4] = [true,1,1,false];
data.grid[col][5] = [false,0,0];
data.grid[col][6] = [false,0,0];
data.grid[col][7] = [true,1,0,false];
data.grid[col][8] = [false,1,0];

col = 9;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [false,0,0];
data.grid[col][2] = [false,0,0];
data.grid[col][3] = [false,0,0];
data.grid[col][4] = [true,0,0,true];
data.grid[col][5] = [true,1,1,true];
data.grid[col][6] = [false,1,1];
data.grid[col][7] = [true,0,0,false];
data.grid[col][8] = [false,1,0];

col = 10;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [true,1,0,true];
data.grid[col][2] = [true,1,1,true];
data.grid[col][3] = [false,1,1];
data.grid[col][4] = [true,1,0,true];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [true,1,1,false];
data.grid[col][7] = [false,0,0];
data.grid[col][8] = [true,0,0,true];

col = 11;
data.grid[col][0] = [false,0,1];
data.grid[col][1] = [false,1,0];
data.grid[col][2] = [true,0,1,true];
data.grid[col][3] = [true,1,0,false];
data.grid[col][4] = [true,0,1,false];
data.grid[col][5] = [true,0,0,false];
data.grid[col][6] = [true,0,1,false];
data.grid[col][7] = [false,1,1];
data.grid[col][8] = [true,1,0,true];

col = 12;
data.grid[col][0] = [false,0,1];
data.grid[col][1] = [true,0,1,false];
data.grid[col][2] = [false,1,1];
data.grid[col][3] = [true,0,0,false];
data.grid[col][4] = [true,1,1,false];
data.grid[col][5] = [true,1,0,false];
data.grid[col][6] = [true,1,1,false];
data.grid[col][7] = [true,1,0,false];
data.grid[col][8] = [false,1,0];

col = 13;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [false,0,0];
data.grid[col][2] = [true,1,0,false];
data.grid[col][3] = [true,1,1,false];
data.grid[col][4] = [true,0,0,true];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [false,1,0];
data.grid[col][7] = [true,0,1,true];
data.grid[col][8] = [true,0,0,true];

col = 14;
data.grid[col][0] = [true,1,1,true];
data.grid[col][1] = [false,1,1];
data.grid[col][2] = [true,0,0,false];
data.grid[col][3] = [true,0,1,false];
data.grid[col][4] = [true,1,0,true];
data.grid[col][5] = [false,0,1];
data.grid[col][6] = [true,0,1,false];
data.grid[col][7] = [false,1,1];
data.grid[col][8] = [true,1,0,true];

col = 15;
data.grid[col][0] = [true,0,1,true];
data.grid[col][1] = [false,0,0];
data.grid[col][2] = [false,0,0];
data.grid[col][3] = [false,0,0];
data.grid[col][4] = [true,0,0,true];
data.grid[col][5] = [true,0,1,true];
data.grid[col][6] = [false,0,0];
data.grid[col][7] = [false,0,0];
data.grid[col][8] = [true,0,0,true];


// achtergrond
// clip = {
//   bars: [
//     { color: '#ffffff', size: 80, }, 
//     { color: '#f8f8f8', size: 80, }],
//   speed: (-160 * 32 * (1/32)) / framesPerLoop,
//   isVertical: false,
//   x: 0, y: 0, height: 80, width: 80,
// };
// data.clips.push({ ...clip });

// gebouw rechts
// clip = {
//   bars: [
//     { color: '#ffffff', size: 43, },
//     { color: '#f8f8f8', size: 2, },
//     { color: '#eeeeee', size: 5, }
//   ],
//   speed: (-50 * 64 * (1/32)) / framesPerLoop,
//   isVertical: true,
//   x: 0, y: 80, height: 80, width: 120,
//   hskew: 0, vskew: 0,
// };
// data.clips.push({ ...clip });

// cirkels
// clip = {
//   type: 'pulsingDotsRectangle',
//   color: '#f8f8f8',
//   radius: 20,
//   speed: 4 / framesPerLoop,
//   x: 80, y: 0, height: 80, width: 160,
// };
// data.clips.push(clip);

// clip = {
//   type: 'randomMatrix',
//   backgroundColor: '#ffffff',
//   colors: ['#f8f8f8', '#eeeeee'],
//   elWidth: 20, elHeight: 20,
//   numToChange: 1, framesPerChange: 1,
//   x: 240, y: 0, width: 300, height: 120,
// };
// data.clips.push(clip);

console.log(data);
