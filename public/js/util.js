export function scaleDefault(data, scale) {
  data.canvas.width *= scale;
  data.canvas.height *= scale;
  data.clips = data.clips.reduce((clipAcc, clip) => {
    return [
      ...clipAcc,
      {
        ...clip, 
        speed: clip.speed * scale,
        x: clip.x * scale,
        y: clip.y * scale,
        height: clip.height * scale,
        width: clip.width * scale,
        bars: clip.bars.reduce((barAcc, bar) => {
          return [ 
            ...barAcc,
            {
              ...bar,
              size: bar.size * scale,
            },];
        }, []),
      }
    ];
  }, []);
}

export function scaleGridAnim(data, scale) {
  data.canvas.width *= scale;
  data.canvas.height *= scale;
  data.gridSize *= scale;
  data.clips = data.clips.reduce((clipAcc, clip) => {

    const c = {
      ...clip,
      speed: clip.speed * scale,
      x: clip.x * scale,
      y: clip.y * scale,
      height: clip.height * scale,
      width: clip.width * scale,
      items: clip.items.reduce((itemAcc, item) => {
        return [ 
          ...itemAcc,
          {
            ...item,
            size: item.size * scale,
          },];
      }, []),
    };

    switch (clip.type) {
      case 'circle':
        c.centerX = clip.centerX * scale;
        c.centerY = clip.centerY * scale;
        c.fixedRadius = clip.fixedRadius * scale;
        break;
      case 'rectangle':
        break;
    }
    return [ ...clipAcc, c];
  }, []);
}
