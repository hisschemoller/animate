// 20 pixels in twee seconden bij 60 fps
// is 20 pixels in 120 frames
// is 1 pixel in 6 frames

// 16 bars van 10 pixels in twee seconden bij 60 fps
// is 160 pixels in 120 frames
// is 160/120 (1.333) pixels per frame

// (itemSize * steps) / (loopLength * fps)
let clip;

const height = 360;
const width = 640;
const loopLength = 2.1;
const numLoops = 64;
const fps = 30;
const data = {
  canvas: { height, width },
  endFrame: Math.ceil(loopLength * fps * numLoops),
  clips: [],
};

// lucht
clip = {
  bars: [{
    color: '#a0deff',
    size: 100,
  }, {
    color: '#afcdd8',
    size: 100,
  }],
  speed: (-200 * 14 * (1/32)) / (loopLength * fps),
  isVertical: false,
  x: 50, y: 0, height: height - 140, width: width - 50,
  hskew: 0, vskew: 0.1,
};
data.clips.push({ ...clip });

// gebouw achterst
clip = {
  bars: [{
    color: '#788490',
    size: 10,
  }, {
    color: '#cbd3c6',
    size: 20,
  }, {
    color: '#f7ddc2',
    size: 3,
  }],
  speed: (33 * 14 * 0.25) / (loopLength * fps),
  isVertical: false,
  x: 240, y: 20, height: height - 20 - 140, width: width - 240,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

// grond hoog
clip = {
  bars: [{
    color: '#a6aab3',
    size: 10,
  }, {
    color: '#616679',
    size: 2,
  }],
  speed: (-12 * 14 * 0.5) / (loopLength * fps),
  isVertical: true,
  x: 140, y: height - 140, height: 55, width: width - 150 - 320 + 30,
  hskew: 0, vskew: -0.3,
};
data.clips.push({ ...clip });

// gebouw midden
clip = {
  bars: [{
    color: '#b1b7b5',
    size: 5,
  }, {
    color: '#739fc2',
    size: 10,
  }, {
    color: '#544d43',
    size: 5,
  }],
  speed: (20 * 14 * (1/4)) / (loopLength * fps),
  isVertical: false,
  x: 0, y: 0, height, width,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip, x: 320, y: 0 + 20, height: height - 85, width: 20, hskew: -1, bars: [{
  color: '#b1b7b5',
  size: 5,
}, {
  color: '#a5bccf',
  size: 10,
}, {
  color: '#696156',
  size: 5,
}] });
data.clips.push({ ...clip, x: 340, y: 0, height: height - 85, width: 100, hskew: 0 });
data.clips.push({ ...clip, x: 440, y: 0, height: height - 85, width: 20, hskew: 1,
  bars: [{
    color: '#b0aea2',
    size: 5,
  }, {
    color: '#544e42',
    size: 10,
  }, {
    color: '#739fc2',
    size: 5,
  }], });

// gebouw midden links
clip = {
  bars: [{
    color: '#889db6',
    size: 7,
  }, {
    color: '#69796c',
    size: 2,
  }, {
    color: '#43493f',
    size: 1,
  }],
  speed: (20 * 14 * (1/8)) / (loopLength * fps),
  isVertical: true,
  x: 100, y: 30, height: height - 115, width: 40,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

// grond midden
clip = {
  bars: [{
    color: '#525a7f',
    size: 10,
  }, {
    color: '#dedd8d',
    size: 3,
  }],
  speed: (-13 * 16) / (loopLength * fps),
  isVertical: false,
  x: 110, y: height - 85, height: 35, width: width - 110 - 220 + 20,
  hskew: 0, vskew: -0.3,
};
data.clips.push({ ...clip });

// gebouw rechts
clip = {
  bars: [{
    color: '#85d7ff',
    size: 10,
  }, {
    color: '#69796c',
    size: 30,
  }, {
    color: '#43493f',
    size: 5,
  }],
  speed: (45 * 14 * (1/4)) / (loopLength * fps),
  isVertical: true,
  x: 0, y: 0, height, width,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip, x: width - 150 - 70, y: 60 + 35, height: height - 60 - 50, width: 70, hskew: -0.5,bars: [{
  color: '#85d7ff',
  size: 10,
}, {
  color: '#7b8c7e',
  size: 30,
}, {
  color: '#43493f',
  size: 5,
}] });
data.clips.push({ ...clip, x: width - 150, y: 60, height: height - 60 - 50, width: 150, hskew: 0 });

// grond onder
clip = {
  bars: [{
    color: '#494e5f',
    size: 50,
  }, {
    color: '#f6e78b',
    size: 5,
  }],
  speed: (-55 * 14 * (1/16)) / (loopLength * fps),
  isVertical: true,
  x: 110, y: height - 50, height: 50, width: width - 110,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

// objecten tweede
clip = {
  bars: [{
    color: '#ffd343',
    size: 25,
  }, {
    color: '#9f4135',
    size: 5,
  }, {
    color: 'transparent',
    size: 360,
  }],
  speed: (390 * 14 * (1/8)) / (loopLength * fps),
  isVertical: false,
  x: 0, y: height - 150, height: 30, width,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

// gebouw links
clip = {
  bars: [{
    color: '#74a2c6',
    size: 10,
  }, {
    color: '#757170',
    size: 30,
  }, {
    color: '#29221c',
    size: 5,
  }],
  speed: (-45 * 14 * 0.25) / (loopLength * fps),
  isVertical: true,
  x: 0, y: 0, height, width,
  hskew: 0, vskew: 0,
};

data.clips.push({ ...clip, width: 50 });
data.clips.push({ ...clip, x: 50, width: 40, hskew: 0.2 });
data.clips.push({ ...clip, x: 90, y: 8, width: 20, hskew: 1, bars: [{
  color: '#74a2c6',
  size: 10,
}, {
  color: '#5d5958',
  size: 30,
}, {
  color: '#29221c',
  size: 5,
}] });

// objecten voor blauw
clip = {
  bars: [{
    color: '#41548f',
    size: 40,
  }, {
    color: 'transparent',
    size: width,
  }, {
    color: '#24303e',
    size: 10,
  }, {
    color: '#d69e8d',
    size: 10,
  }, {
    color: 'transparent',
    size: width,
  }],
  speed: ((width + 40) * 14 * (1/8)) / (loopLength * fps),
  isVertical: false,
  x: 0, y: height - 100, height: 40, width,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

// objecten voor
clip = {
  bars: [{
    color: '#000',
    size: 10,
  }, {
    color: 'transparent',
    size: width,
  }],
  speed: ((width + 10) * 16 * (1/8)) / (loopLength * fps),
  isVertical: false,
  x: 0, y: height - 180, height: 150, width,
  hskew: 0, vskew: 0,
};
data.clips.push({ ...clip });

export default data;

export function scale(scale) {
  data.canvas.width *= scale;
  data.canvas.height *= scale;
  data.clips = data.clips.reduce((clipAcc, clip) => {
    return [
      ...clipAcc,
      {
        ...clip, 
        speed: clip.speed * scale,
        x: clip.x * scale,
        y: clip.y * scale,
        height: clip.height * scale,
        width: clip.width * scale,
        bars: clip.bars.reduce((barAcc, bar) => {
          return [ 
            ...barAcc,
            {
              ...bar,
              size: bar.size * scale,
            },];
        }, []),
      }
    ];
  }, []);
}
