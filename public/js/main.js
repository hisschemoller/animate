import init from './player.js';
import { scaleDefault, scaleGridAnim }  from './util.js';

init({
  isCapture: true,
  scale: 2,
  scaleFunction: scaleGridAnim,
});
