
export default function createRectangleClip(data) {
  let canvas,
    ctx,
    animCanvas,
    animCtx,
    itemsSize,

    init = function() {
      const {width, height, items} = data;

      canvas = document.createElement('canvas');
      canvas.height = height;
      canvas.width = width;
      ctx = canvas.getContext('2d');

      animCanvas = document.createElement('canvas');
      animCtx = animCanvas.getContext('2d');
      itemsSize = items.reduce((acc, item) => acc + item.size, 0);
      const repeats = Math.ceil(width / itemsSize) + 2;
      const canvasSize = repeats * itemsSize;
      animCanvas.height = height;
      animCanvas.width = canvasSize;

      for (let i = 0; i < repeats; i++) {
        let itemSize = 0;
        for (let j = 0; j < items.length; j++) {

          // i don't know, but it fixes the color sequence
          const itemIndex = j === 0 ? items.length - 1 : j - 1;
          const { color, size } = items[itemIndex];
          const x = (i * itemsSize) + itemsSize - itemSize;
          itemSize += size;
          animCtx.fillStyle = color;
          animCtx.fillRect(x, 0, size, height);
        }
      }
    },

    getCanvas = function() {
      return canvas;
    },

    drawCanvas = function(frame) {
      const { height, width, speed } = data;

      let x = 0;
      let y = 0;
      if (speed < 0) {
        x = (-speed * frame) % itemsSize;
      } else if (speed > 0) {
        x = ((-speed * frame) % itemsSize) + itemsSize * 2;
      }

      ctx.drawImage(animCanvas, 
        x, y, width, height, 
        0, 0, width, height);
    },

    draw = function(mainCtx, frame) {
      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      drawCanvas(frame);
      mainCtx.drawImage(canvas, 0, 0);
      mainCtx.restore();
    };
    
  init();

  return {
    draw,
    drawCanvas,
    getCanvas,
  };
}
