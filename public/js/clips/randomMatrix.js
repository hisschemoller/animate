
export default function createRandomMatrixClip(data) {
  let canvas,
    ctx,
    cols,
    rows,

    init = function() {
      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');
      canvas.height = data.height;
      canvas.width = data.width;
      cols = Math.floor(data.width / data.elWidth);
      rows = Math.floor(data.height / data.elHeight);

      // fill 
      for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
          const x = col * data.elWidth;
          const y = row * data.elHeight;
          drawElement(x, y);
        }
      }
    },

    drawElement = function(x, y) {
      ctx.fillStyle = data.backgroundColor;
      ctx.fillRect(x, y, data.elWidth, data.elHeight);
      if (Math.random() > 0.5) {
        ctx.fillStyle = data.colors[Math.floor(Math.random() * data.colors.length)];
        ctx.fillRect(x, y, data.elWidth, data.elHeight);
      }
    },

    draw = function(mainCtx, frame) {
      if (frame % data.framesPerChange === 0) {
        for (let i = 0; i < data.numToChange; i++) {
          const x = Math.floor(Math.random() * cols) * data.elWidth;
          const y = Math.floor(Math.random() * rows) * data.elHeight;
          drawElement(x, y);
        }
      }

      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      mainCtx.drawImage(canvas, 0, 0);
      mainCtx.restore();
    };

  init();

  return {
    draw,
  };
}
