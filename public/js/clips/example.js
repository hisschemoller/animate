
export default function createExampleClip(data) {
  let canvas,
    ctx,

    init = function() {
      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');
      canvas.height = data.height;
      canvas.width = data.width;
    },

    draw = function(mainCtx, frame) {
      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      mainCtx.drawImage(canvas, 0, 0);
      mainCtx.restore();
    };

  init();

  return {
    draw,
  };
}
