
export default function createCircleClip(data) {
  let canvas,
    ctx,
    dataItemIndex = 0, // index in data.items, which ring to add
    ringDrawIndex = 0, // index of the outer ring, the first to draw
    ringLastDrawIndex = 0, // index of the inner ring, the last to draw
    ringAddIndex = 0, // index of position of next
    rings = [],
    outerRadius,
    doublePI = Math.PI * 2,

    init = function() {
      const {centerX, centerY, width, height, items, fixedRadius} = data;

      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');
      canvas.height = height;
      canvas.width = width;

      if (fixedRadius) {
        outerRadius = fixedRadius;
      } else {
        const x = 0;
        const y = 0;
        const x2 = x + width;
        const y2 = y + height;
        const lt = Math.sqrt((x - centerX) ** 2 + (y - centerY) ** 2);
        const rt = Math.sqrt((x2 - centerX) ** 2 + (y - centerY) ** 2);
        const lb = Math.sqrt((x - centerX) ** 2 + (y2 - centerY) ** 2);
        const rb = Math.sqrt((x2 - centerX) ** 2 + (y2 - centerY) ** 2);
        outerRadius = Math.max(lt, rt, lb, rb);
      }

      // determine the number of rings to always cover the whole circle
      const itemsSize = items.reduce((acc, item) => acc + item.size, 0);
      const numItems = Math.ceil(outerRadius / itemsSize);
      const arbitraryExtraRings = 2;
      const numRings = items.length * numItems + arbitraryExtraRings;
      
      // create the ring data array
      for (let i = 0; i < numRings; i++) {
        rings.push({
          isActive: false,
        });
      }

      dataItemIndex = data.speed > 0 ? 0 : data.items.length - 1;

      // set the initial active rings, from small to large
      let ringIndex = ringDrawIndex;
      let radius = outerRadius;
      while (radius > 0) {
        const ringData = { ...items[dataItemIndex], isActive: true, radius};
        rings[ringIndex] = ringData;
        
        // update counters
        changeDataItemIndex(data.speed > 0);
        ringLastDrawIndex = ringIndex;
        ringIndex = (ringIndex + 1) % rings.length;
        radius -= ringData.size;
      }

      // i don't know, but it fixes the color sequence
      if (data.speed < 0) {
        changeDataItemIndex(data.speed < 0);
      }
      
      // clip to fixed radius
      if (fixedRadius) {
        ctx.beginPath();
        ctx.arc(centerX, centerY, fixedRadius, 0, doublePI);
        ctx.clip();
      }
    },

    changeDataItemIndex = function(isChangeForward) {
      if (isChangeForward) {
        dataItemIndex = (dataItemIndex + 1) % data.items.length
      } else {
        dataItemIndex = dataItemIndex === 0 ? data.items.length - 1 : dataItemIndex - 1;
      }
    },

    drawCanvas = function() {
      const { centerX, centerY, items, speed, backgroundColor, fixedRadius } = data;
      
      // draw background 
      if (!fixedRadius && backgroundColor) {
        ctx.fillStyle = backgroundColor;
        ctx.fillRect(0, 0, data.width, data.height);
      }

      // draw circle
      for (let i = 0, n = rings.length; i < n; i++) {
        const drawIndex = (ringDrawIndex + i) % rings.length;
        const ring = rings[drawIndex];

        // draw single ring
        if (ring.isActive) {
          ctx.fillStyle = ring.color;
          ctx.beginPath();
          ctx.arc(centerX, centerY, ring.radius, 0, doublePI);
          ctx.fill();

          // update radius for next draw
          ring.radius += speed;
          // ring.tubeRadius = Math.sin(Math.max(0, Math.min(ring.radius / outerRadius, 1)) * Math.PI) * outerRadius;
        }
      }

      // check if rings can be removed
      for (let i = 0, n = rings.length; i < n; i++) {
        const ring = rings[i];
        if (ring.isActive) {
          if (speed > 0 && ring.radius - ring.size - speed > outerRadius) {

            // circle grows and outer ring can be removed
            ring.isActive = false;

            // if the widest ring is removed, the ringDrawIndex
            // changes to the next widest wing, because they're drawn
            // from large to small
            ringDrawIndex = (ringDrawIndex + 1) % rings.length;
          } else if (speed < 0 && ring.radius + speed <= 0) {

            // circle shrinks and inner ring can be removed
            ring.isActive = false;
            
            // if the smallest ring is removed, ringLastDrawIndex
            // changes to the next smallest ring
            ringLastDrawIndex = ringLastDrawIndex === 0 ? rings.length - 1 : ringLastDrawIndex - 1;
          }
        }
      }

      // check if new rings must be added
      if (speed > 0) {
        const innerRing = rings[ringLastDrawIndex];
        if (innerRing.radius > innerRing.size) {

          // get data for the new ring
          const ringData = items[dataItemIndex];
          changeDataItemIndex(data.speed > 0);

          // index of the new ring
          ringLastDrawIndex = (ringLastDrawIndex + 1) % rings.length;

          // set the new ring
          const newInnerRing = rings[ringLastDrawIndex];
          newInnerRing.size = ringData.size;
          newInnerRing.color = ringData.color;
          newInnerRing.radius = innerRing.radius - innerRing.size;
          newInnerRing.isActive = true;
        }
      } else if (speed < 0) {
        const outerRing = rings[ringDrawIndex];
        if (outerRing.radius < outerRadius) {

          // get data for the new ring
          const ringData = items[dataItemIndex];
          changeDataItemIndex(data.speed < 0);

          // index of the new ring
          ringDrawIndex = ringDrawIndex === 0 ? rings.length - 1 : ringDrawIndex - 1;

          // set the new ring
          const newOuterRing = rings[ringDrawIndex];
          newOuterRing.size = ringData.size;
          newOuterRing.color = ringData.color;
          newOuterRing.radius = outerRing.radius + ringData.size;
          newOuterRing.isActive = true;
        }
      }
    },

    getCanvas = function() {
      return canvas;
    },

    draw = function(mainCtx, frame) {
      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      drawCanvas();
      mainCtx.drawImage(canvas, 0, 0);
      mainCtx.restore();
    };

  init();

  return {
    draw,
    drawCanvas,
    getCanvas,
  };
}
