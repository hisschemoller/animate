
/**
 * 
 * @param {Object} data
 * @param {Number} data.x
 * @param {Number} data.y
 * @param {Number} data.width
 * @param {Number} data.height
 * @param {String[]} data.colors
 * @param {Number[]} data.vector
 */
export default function createClip(data) {
  let canvas,
    ctx,
    itemSize,

    init = function() {
      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');
      itemSize = data.bars.reduce((acc, bar) => acc + bar.size, 0);
      
      if (data.isVertical) {
        const repeats = Math.ceil(data.height / itemSize) + 2;
        const canvasSize = repeats * itemSize;
        canvas.height = canvasSize;
        canvas.width = data.width;
        for (let i = 0; i < repeats; i++) {
          let barSize = 0;
          for (let j = 0; j < data.bars.length; j++) {
            const { size } = data.bars[j];
            const y = (i * itemSize) + barSize;
            barSize += size;
            ctx.fillStyle = data.bars[j].color;
            ctx.fillRect(0, y, data.width, size);
          }
        }
      } else {
        const repeats = Math.ceil(data.width / itemSize) + 2;
        const canvasSize = repeats * itemSize;
        canvas.height = data.height;
        canvas.width = canvasSize;
        for (let i = 0; i < repeats; i++) {
          let barSize = 0;
          for (let j = 0; j < data.bars.length; j++) {
            const { size } = data.bars[j];
            const x = (i * itemSize) + barSize;
            barSize += size;
            ctx.fillStyle = data.bars[j].color;
            ctx.fillRect(x, 0, size, data.height);
          }
        }
      }
    },
    
    draw = function(mainCtx, frame) {
      let x = 0;
      let y = 0;
      if (data.isVertical) {
        if (data.speed < 0) {
          y = (-data.speed * frame) % itemSize;
        } else if (data.speed > 0) {
          y = ((-data.speed * frame) % itemSize) + itemSize * 2;
        }
      } else {
        if (data.speed < 0) {
          x = (-data.speed * frame) % itemSize;
        } else if (data.speed > 0) {
          x = ((-data.speed * frame) % itemSize) + itemSize * 2;
        }
      }
      
      mainCtx.save();
      mainCtx.transform(1, data.hskew, data.vskew, 1, data.x, data.y);
      mainCtx.drawImage(canvas, 
        x, y, data.width, data.height, 
        0, 0, data.width, data.height);
      mainCtx.restore();
    };
    
  init();

  return {
    draw,
  };
}
