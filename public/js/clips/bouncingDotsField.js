
export default function createBDFClip(data) {
  let canvas,
    ctx,
    dots = [],

    init = function() {
      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');

      canvas.height = data.height;
      canvas.width = data.width + data.radius;

      for (let i = 0, n = data.amount; i < n; i++) {
        const x = Math.random() * data.width;
        const y = data.height - data.radius - (Math.random() * 20);
        dots.push({
          floor: { x, y, },
          now: { x, y: y - (Math.random() * 80) },
          speed: data.speed,
        });
      }
      console.log(dots);
    },
    
    draw = function(mainCtx, frame) {
      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      mainCtx.fillStyle = data.color;
      dots.forEach(dot => {
        dot.speed += data.gravity;
        if (dot.now.y + dot.speed > dot.floor.y) {
          dot.now.y = dot.floor.y - (dot.now.y + dot.speed - dot.floor.y);
          dot.speed = dot.speed * -1;
        } else {
          dot.now.y += dot.speed;
        }

        mainCtx.beginPath();
        mainCtx.arc(dot.now.x, dot.now.y, data.radius, 0, Math.PI * 2, true);
        mainCtx.fill();
      });
      mainCtx.restore();
    };
    
  init();

  return {
    draw,
  };
}
