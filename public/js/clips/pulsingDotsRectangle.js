
export default function createPDRClip(data) {
  let canvas,
    ctx,
    cols,
    rows,
    dots = [],

    init = function() {
      canvas = document.createElement('canvas');
      ctx = canvas.getContext('2d');

      canvas.height = data.height;
      canvas.width = data.width;
      cols = Math.floor(data.width / (data.radius * 2));
      rows = Math.floor(data.height / (data.radius * 2));

      for (let row = 0, m = rows; row < m; row++) {
        dots.push([]);
        for (let col = 0, n = cols; col < n; col++) {
          dots[row].push({
            x: (col * data.radius * 2) + data.radius,
            y: (row * data.radius * 2) + data.radius,
            phase: Math.random() * Math.PI,
          });
        }
      }
    },
    
    draw = function(mainCtx, frame) {
      mainCtx.save();
      mainCtx.transform(1, 0, 0, 1, data.x, data.y);
      mainCtx.fillStyle = data.color;
      for (let row = 0, m = rows; row < m; row++) {
        for (let col = 0, n = cols; col < n; col++) {
          const dot = dots[row][col];
          const radius = Math.abs(Math.sin(dot.phase)) * data.radius;
          mainCtx.beginPath();
          mainCtx.arc(dot.x, dot.y, radius, 0, Math.PI * 2, true);
          mainCtx.fill();
          dot.phase += data.speed;
        }
      }
      mainCtx.restore();
    };
    
  init();

  return {
    draw,
  };
}
