// read .env files
require('dotenv').config();

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const fs = require('fs');
const port = process.env.PORT || 3006;

// Listen for HTTP requests on port 3000
server.listen(port, () => {
  console.log('listening on %d', port);
});

app.get('public/', function (req, res) {
  res.sendFile(__dirname + 'index.html');
});

// Set public folder as root
app.use(express.static('public'));

// Allow front-end access to node_modules folder
app.use('/scripts', express.static(`${__dirname}/node_modules/`));

io.sockets.on('connection', function (socket) {
  socket.on('render-frame', function (data) {
    // Pad frame number with zreos so it's four characters in length.
    data.frame = (data.frame <= 99999) ? ('0000' + data.frame).slice(-5) : '99999';
    // Get rid of the data:image/png;base64 at the beginning of the file data
    data.file = data.file.split(',')[1];
    var buffer = new Buffer(data.file, 'base64');
    fs.writeFile(__dirname + '/tmp/frame_' + data.frame + '.png',
      buffer.toString('binary'),
      'binary',
      (err) => {
        if (err) {
          console.log('An error occurred: ', err);
          throw err;
        }
      });
  });
});
