# animate

Javascript canvas animation.

## FFMPEG

### Render video from PNG image sequence

```
ffmpeg -framerate 30 -i tmp/frame_%05d.png -f mp4 -vcodec libx264 -pix_fmt yuv420p output.mp4
```

This one works in Quicktime.

### Combine video and audio files

Where 134.5 in `enable='gt(t\,134.5)'` is time in seconds.

```
ffmpeg -i video_output.mp4 -i audio_output.wav -vf drawbox=c=Black:t=fill:enable='gt(t\,134)' -vcodec libx264 -acodec aac output.mp4
```

Drawbox filter to add black frames at the end of the video if the audio is longer.

enable='gt(t\,134)' starts the drawbox after 134 seconds, which was the video duration in this case.

AAC audio codec works for Quicktime on MacOS High Sierra.